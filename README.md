KenCryptopierre
A [KenCryptopierre] é um script em NodeJS que carregar informações atualizadas de crypto moedas através da API CoinMarketCap.

Instalação/Utilização
Para ter acesso às funcionalidade do script, faça a instalação do pacote que está no NPM

Com YARN

```json
yarn add kenCryptopierre
```

ou NPM

```json
npm i kenCryptopierre
```

Funções:

 Cotação atualizada da crypto moeda selecionada

```json
cryptos = new KenCryptoPierre()
cryptos.quote(['btc']).the((resp) => {
  console.log(resp)
})
```

Caso tudo der certo, o formato da resposta será esse:

{
  id: 1,
  symbol: 'BTC',
  name: 'Bitcoin',
  amount: 1,
  last_updated: '2022-01-25T16:54:00.000Z',
  quote: {
    BRL: {
      price: 202462.6530063235,
      last_updated: '2022-01-25T16:54:02.000Z'
    }
  }
}


Caso a requisição falhe, o resultado será esse: 

```json 
undefined
```


Conversão de crypto moeda em outras moedas correntes:

```json
cryptos = new KenCryptoPierre()
cryptos.covert(['btc'], 1, ['brl']).the((resp) => {
  console.log(resp)
})
```

Caso de tudo certo, o formato da resposta será esse:

```json

{
  id: 1,
  symbol: 'BTC',
  name: 'Bitcoin',
  amount: 1,
  last_updated: '2022-01-25T16:54:00.000Z',
  quote: {
    BRL: {
      price: 202462.6530063235,
      last_updated: '2022-01-25T16:54:02.000Z'
    }
  }
}
```

Caso a requisição falhe, o resultado será esse: 

```json 
undefined
```

