import axios, { AxiosInstance, AxiosResponse } from "axios";
import { Convert, InvalidData } from "../types/typesCrypto";
import dotenv from "dotenv";
import { formatQuotes } from "../services/normalize";

dotenv.config();
const key = process.env.API_KEY;

export default class KenCryptoPierre {
  baseURL: string = `https://pro-api.coinmarketcap.com`;
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
    });
  }

  async quotes(symbol: Array<string>) {
    const requestURL = `/v1/cryptocurrency/quotes/latest?symbol=${symbol}&CMC_PRO_API_KEY=${key}`;
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestURL
      );
      const bitInfo = formatQuotes(response.data);

      return bitInfo;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as InvalidData;
      }
    }
  }

  async convert(symbol: string, amount: number, convert: string) {
    const requestURL = `/v1/tools/price-conversion?symbol=${symbol}&amount=${amount}&convert=${convert}&CMC_PRO_API_KEY=${key}`;
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestURL
      );
      return response.data.data as Convert;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as InvalidData;
      }
    }
  }
}
