export interface USD {
  [key: string]: {
    price: number;
    last_updated: Date;
  };
}
export interface Quote {
  USD: USD;
}

export interface BTCinfo {
  id: number;
  name: string;
  symbol: string;
  slug: string;
  is_active: number;
  is_fiat: number;
  circulating_supply: number;
  total_supply: number;
  max_supply: number;
  date_added: string;
  num_market_pairs: number;
  cmc_rank: number;
  last_updated: Date;
  tags: Array<string>;
  platform: null;
  quote: Quote;
}

export interface Data {
  BTC: BTCinfo;
}
export interface CryptoResponse {
  status: {
    timestamp: string;
    error_code: number;
    error_message: number;
    elapsed: number;
    credit_count: number;
    notice: number;
    total_count: number;
  };
  data: Data;
}

export interface QuoteConvert {
  price: number;
  last_updated: Date;
}

export interface Convert {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: Date;
  quote: QuoteConvert;
}

export interface InvalidData {
  status: {
    error_message: string | null;
  };
}

export function isError(error: any) {
  return (error as InvalidData).status.error_message !== null;
}
